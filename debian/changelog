luarocks (3.8.0+dfsg1-1) unstable; urgency=medium

  * New upstream version 3.8.0+dfsg1
  * Basic test, thanks Chris MacNaughton
  * Support multiple lua versions in /usr/local (Closes:#996781)
  * Lessen configuration flags, luarocks is smarter now.
  * Install /usr/bin/luarocks(-admin)-5.x helpers

 -- Jérémy Lal <kapouer@melix.org>  Wed, 10 Nov 2021 15:35:37 +0100

luarocks (3.7.0+dfsg1-1) unstable; urgency=medium

  * New upstream release (Closes: #909394, #930257, #995240)
  * Add local-tree to luarocks-admin
  * AMAU, lua-team maintenance
  * Standards-Version 4.5.1
  * Update Vcs fields
  * Use watch file version 4
  * Exclude binary/ files
  * Update copyright to format 1.0
  * Fix stars in NEWS
  * Less intervention for luarocks paths resolution
  * debhelper 13

 -- Jérémy Lal <kapouer@melix.org>  Tue, 28 Sep 2021 17:01:22 +0200

luarocks (2.4.2+dfsg-1.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Sat, 02 Jan 2021 00:36:28 +0100

luarocks (2.4.2+dfsg-1) unstable; urgency=medium

  * New upstream release
  * Support lua 5.1, 5.2 and 5.3 via lua-any
  * Recommend lua-sec for https download

 -- Enrico Tassi <gareuselesinge@debian.org>  Mon, 26 Dec 2016 10:15:35 +0100

luarocks (2.2.0+dfsg-2) unstable; urgency=medium

  * Build depend on lua5.1 explicitly (Close: #796443)

 -- Enrico Tassi <gareuselesinge@debian.org>  Sat, 22 Aug 2015 17:48:25 +0200

luarocks (2.2.0+dfsg-1) unstable; urgency=medium

  * New upstream release
  * patch 0001-Fixed-detection-of-Debian-paths removed (applied upstream)

 -- Enrico Tassi <gareuselesinge@debian.org>  Sat, 11 Oct 2014 15:26:47 +0200

luarocks (2.0.12-2) unstable; urgency=low

  * Add to the config file the correct multiarch path for system
    wide libraries (Closes: #724509)

 -- Enrico Tassi <gareuselesinge@debian.org>  Tue, 24 Sep 2013 22:31:04 +0200

luarocks (2.0.12-1) unstable; urgency=low

  * New upstream release
  * Cherry-pick upstream patch for Debian paths

 -- Enrico Tassi <gareuselesinge@debian.org>  Sun, 09 Jun 2013 17:50:48 +0200

luarocks (2.0.9-1) unstable; urgency=low

  * New upstream release with some support for Lua 5.2

 -- Enrico Tassi <gareuselesinge@debian.org>  Sun, 15 Apr 2012 14:19:13 +0200

luarocks (2.0.8-2) unstable; urgency=low

  * Clen up luarocks manifest files in postrm script (Closes: #668660)

 -- Enrico Tassi <gareuselesinge@debian.org>  Sun, 15 Apr 2012 14:14:22 +0200

luarocks (2.0.8-1) unstable; urgency=low

  * Moved from CDBS to dh7
  * Source format 3.0 (quilt)
  * Set debian/compat to 7
  * Bumped standards-version to 3.9.3
  * New upstream release (Closes: #656194)
  * Hardcode lua interpreter to lua5.1 (Closes: #6508660)
  * Probe system at run time calling io.popen('uname') (Closes: #663695)

 -- Enrico Tassi <gareuselesinge@debian.org>  Sat, 24 Mar 2012 19:06:24 +0100

luarocks (2.0.2-1) unstable; urgency=low

  * New upstream release
  * updated debian/rules not to install rcluancher
  * updated standards version to 3.8.4, no changes
  * added debian/source/format -- stil 1.0

 -- Enrico Tassi <gareuselesinge@debian.org>  Tue, 20 Apr 2010 09:34:55 +0200

luarocks (2.0.1-3) unstable; urgency=low

  * create /usr/local/lib/luarocks/rocks during postinst (Closes: #564153)

 -- Enrico Tassi <gareuselesinge@debian.org>  Tue, 12 Jan 2010 23:41:05 +0100

luarocks (2.0.1-2) unstable; urgency=low

  * do not install empty directory /usr/lib/luarocks (Closes: #555008)
  * run luarocks-admin make_manifest in postinst (Closes: #555007)

 -- Enrico Tassi <gareuselesinge@debian.org>  Sun, 08 Nov 2009 00:01:44 +0100

luarocks (2.0.1-1) unstable; urgency=low

  [ Duboucher Thomas ]
  * new upstream release
  * updated watch to http://www.luarocks.org/releases/
  * changed lua to Lua in package description
  * changed luarocks to LuaRocks in package description

  [ Enrico Tassi ]
  * bumped standards-version to 3.8.3, no changes
  * install rocks in /usr/local
  * added manpages for luarocks and luarocks-admin

 -- Enrico Tassi <gareuselesinge@debian.org>  Sun, 01 Nov 2009 17:33:21 +0100

luarocks (1.0.1-1) unstable; urgency=low

  * new upstream release
  * added misc:Depends to Depends:
  * bumped standards-version to 3.8.1, no changes needed
  * added dependency over zip package (Closes: #497830)

 -- Enrico Tassi <gareuselesinge@debian.org>  Mon, 23 Mar 2009 11:14:11 +0100

luarocks (1.0-2) unstable; urgency=low

  * upload to unstable

 -- Enrico Tassi <gareuselesinge@debian.org>  Sun, 15 Mar 2009 10:23:03 +0100

luarocks (1.0-1) experimental; urgency=low

  * New upstream release
  * Bumped Standards-Version to 3.8.0, no changes needed

 -- Enrico Tassi <gareuselesinge@debian.org>  Wed, 03 Sep 2008 18:02:21 +0200

luarocks (0.6.0.2-1) unstable; urgency=low

  * New upstream release

 -- Enrico Tassi <gareuselesinge@debian.org>  Wed, 02 Jul 2008 10:22:44 +0200

luarocks (0.5.2-1) unstable; urgency=low

  * New upstream release

 -- Enrico Tassi <gareuselesinge@debian.org>  Wed, 14 May 2008 11:55:11 +0200

luarocks (0.5.1-1) unstable; urgency=low

  * New upstream release featuring minor bugfixes

 -- Enrico Tassi <gareuselesinge@debian.org>  Sat, 26 Apr 2008 11:52:15 +0200

luarocks (0.5-1) unstable; urgency=low

  * New upstream release

 -- Enrico Tassi <gareuselesinge@debian.org>  Fri, 04 Apr 2008 10:57:58 +0200

luarocks (0.4.3-2) unstable; urgency=low

  * Added build dependency on liblua5.1-dev, that is not actually used, but
    the configure script checks its existence (Closes: #470267)

 -- Enrico Tassi <gareuselesinge@debian.org>  Mon, 10 Mar 2008 13:17:03 +0100

luarocks (0.4.3-1) unstable; urgency=low

  * Initial release. (Closes: #465124)

 -- Enrico Tassi <gareuselesinge@debian.org>  Mon, 03 Mar 2008 23:40:03 +0100

#!/usr/bin/make -f

DEB_DESTDIR=debian/tmp
LUA_VERSIONS=5.1 5.2 5.3 5.4

%:
	dh $@

override_dh_auto_configure:
	./configure \
 		--prefix=/usr \
 		--with-lua=/usr \
 		--with-lua-lib=/usr/local/lib \
		--rocks-tree=/usr/local/

override_dh_auto_build:
	make build
	txt2man -t "luarocks 1" -r "LuaRocks" -v "" debian/luarocks.1.txt \
		> luarocks.1
	txt2man -t "luarocks-admin 1" -r "LuaRocks repository administration"\
	       	-v "" debian/luarocks-admin.1.txt > luarocks-admin.1

define LUAROCKS_BIN
#!/bin/sh
v=$${0#*-}
exec /usr/bin/lua$${v} /usr/bin/luarocks --lua-version=$${v} $$@
endef
export LUAROCKS_BIN

define LUAROCKS_ADMIN_BIN
#!/bin/sh
v=$${0#*-*-}
exec /usr/bin/lua$${v} /usr/bin/luarocks-admin --lua-version=$${v} $$@
endef
export LUAROCKS_ADMIN_BIN

define LUAROCKS_CONF
rocks_trees = {
   home..[[/.luarocks]],
   [[/usr/local]]
}
endef
export LUAROCKS_CONF

override_dh_auto_install:
	mkdir -p $(DEB_DESTDIR)/etc/luarocks/
	mkdir -p $(DEB_DESTDIR)/usr/share/man/man1
	echo "$$LUAROCKS_CONF" > $(DEB_DESTDIR)/etc/luarocks/config.lua
	mkdir -p $(DEB_DESTDIR)/usr/bin/
	cp src/bin/luarocks $(DEB_DESTDIR)/usr/bin/
	cp src/bin/luarocks-admin $(DEB_DESTDIR)/usr/bin/
	sed -i -e '1c\#!/usr/bin/env lua-any\n-- Lua-Versions: $(LUA_VERSIONS)' $(DEB_DESTDIR)/usr/bin/*
	sed -i -e '3d' $(DEB_DESTDIR)/usr/bin/*
	for V in $(LUA_VERSIONS); do\
	  mkdir -p $(DEB_DESTDIR)/usr/share/lua/$$V/;\
	  cp -r src/luarocks $(DEB_DESTDIR)/usr/share/lua/$$V/;\
		cp $(DEB_DESTDIR)/etc/luarocks/config.lua $(DEB_DESTDIR)/etc/luarocks/config-$$V.lua;\
		echo "$$LUAROCKS_BIN" > $(DEB_DESTDIR)/usr/bin/luarocks-$$V;\
		echo "$$LUAROCKS_ADMIN_BIN" > $(DEB_DESTDIR)/usr/bin/luarocks-admin-$$V;\
		ln -sf luarocks.1 $(DEB_DESTDIR)/usr/share/man/man1/luarocks-$${V}.1;\
		ln -sf luarocks-admin.1 $(DEB_DESTDIR)/usr/share/man/man1/luarocks-admin-$${V}.1;\
	done
	dh_lua -pluarocks -P$(DEB_DESTDIR)

override_dh_auto_clean:
	rm -f luarocks.1 luarocks-admin.1 .gitignore lua config.unix luarocks luarocks-admin
	rm -rf build .luarocks
